import pyaudio
import numpy
from tkinter import *

CHUNK = 1024
WIDTH = 2
CHANNELS = 1
RATE = 48000
RECORD_SECONDS = 20

p = pyaudio.PyAudio()

stream = p.open(format=p.get_format_from_width(WIDTH),
                channels=CHANNELS,
                rate=RATE,
                input=True,
                output=True,
                frames_per_buffer=CHUNK)

print("* recording")

master = Tk()
w = Canvas(master, width=200, height=100)
w.pack()
mainloop()

for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
    data = stream.read(CHUNK)

    data_dft = numpy.fft.fft(data)
    data_dft_abs = [abs(x) for x in data_dft]

    w.create_rectangle(50, 25, 150, 75, fill="blue")

print("* done")

stream.stop_stream()
stream.close()

p.terminate()